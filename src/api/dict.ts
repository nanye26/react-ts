import request from "@/common/ajax";
import { LogApi, ResponseData } from "@/types/api"

class DictApi {

  // 根据id查询操作日志
  selectLogById(data: object) {
    return request.get('/product/sanfu/plm/plmLog/select/selectLogById', data) as Promise<LogApi>;
  }

  // Plm查询数据字典
  async getDict(id: number) {
    return request.get(`/system/sanfu/sys/company/dict/select/Plm/by/type?type=${id}`) as Promise<ResponseData>
  }

}

export default new DictApi()