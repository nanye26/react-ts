import request from "@/common/ajax";
import { LoginData } from "@/types"

class LoginApi {
  login(data: any) {
    return request.post("system/sanfu/ulp/sys/user/login", data) as Promise<LoginData>
  }
}

export default new LoginApi()