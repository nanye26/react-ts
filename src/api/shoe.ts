import request from "@/common/ajax";
import { ListData } from "@/types"

class ShoeApi {
  getList(data: any) {
    return request.post("/product/sanfu/plm/shoeCostConfiguration/query/shoecraft", data) as Promise<ListData>
  }

  // 鞋成本配置导出

  export(data: object) {
    return request({
      url: '/product/sanfu/plm/shoeCostConfiguration/export',
      method: 'post',
      data
    })
  }

  //鞋成本状态停用启用
  updateStatus(data: any) {
    return request({
      url: `/product/sanfu/plm/shoeCostConfiguration/shoe/updateSwitch?type=${data.type}`,
      method: 'post',
      data: data.ids
    })
  }

}

export default new ShoeApi()