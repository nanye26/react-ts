import { useEffect, useState } from "react";
import { Form, Button, Row, Col, Spin, message, Modal } from "antd";
import { ExclamationCircleFilled } from '@ant-design/icons';
import MyPagination, { PageInfo } from "@/components/pagination";
import MyTable from "@/components/table";
import ShoeApi from "@/api/shoe";
import DictApi from "@/api/dict";
import "./index.less";
import { MessageList } from "@/types"
import { Column } from './types'
import MySearch from "@/components/search";
import MyDrawer from "@/components/drawer";
import Router from "@/router";
import { useNavigate, useLocation, useParams } from 'react-router-dom'


const { confirm } = Modal;
export default function ShoeCostList() {

  const [searchForm] = Form.useForm();
  const [pageData, setPageData] = useState<PageInfo>({ page: 1, pageSize: 20 });
  const [tableData, setData] = useState<MessageList>([]);
  const [load, setLoad] = useState(true);
  const [total, setTotal] = useState(0);
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([])
  const [selectData, setSelectData] = useState<object[]>([])
  const [open, setOpen] = useState(false);
  const [id, setId] = useState('')
  const [craftOption, setCraftOption] = useState([])
  const [packOption, setPackOption] = useState([])
  const [packNumOption, setPackNumOption] = useState([])
  const [transportOption, setTransportOption] = useState([])

  const navigate = useNavigate()
  // // /* 手写实现获取到url中？后的参数信息 */
  // let localtion = useLocation()
  // let params = useParams()
  // console.log(localtion, params, "--------------");
  // // //localtion路由信息对象
  // // //params针对于state传参
  const searchData = [
    { type: 'input', prop: 'shoeBoxMaterialSku', label: '鞋盒物料SKU', placeholder: '请输入鞋盒物料SKU' },
    { type: 'input', prop: 'shoeBoxMaterialName', label: '鞋盒物料名称', placeholder: '请输入鞋盒物料名称' },
    { type: 'input', prop: 'shoeBoxSpecification', label: '内盒规格', placeholder: '请输入内盒规格' },
    {
      type: 'select',
      prop: 'processCategory',
      label: '工艺类别',
      placeholder: '请选择工艺类别',
      multiple: true,
      labelName: 'companyDictName',
      valueName: 'companyDictId',
      options: craftOption,
    },
    {
      type: 'select',
      prop: 'packCategory',
      label: '包装类型',
      placeholder: '请选择包装类型',
      multiple: true,
      labelName: 'companyDictName',
      valueName: 'companyDictId',
      options: packOption,
    },
    {
      type: 'select',
      prop: 'shoePackCount',
      label: '包装数量',
      placeholder: '请选择包装数量',
      multiple: true,
      labelName: 'companyDictName',
      valueName: 'companyDictId',
      options: packNumOption,
    },
    {
      type: 'select',
      prop: 'transportationRoute',
      label: '运输路线',
      placeholder: '请选择运输路线',
      multiple: true,
      labelName: 'companyDictName',
      valueName: 'companyDictId',
      options: transportOption,
    },
    { type: 'input', prop: 'outBoxSpecification', label: '外箱规格', placeholder: '请输入外箱规格' },
    {
      type: 'select',
      prop: 'shoeCostStatus',
      label: '状态',
      placeholder: '请选择状态',
      options: [
        { value: '', label: '全部' },
        { value: "1", label: '启用' },
        { value: "0", label: '停用' },
      ],
    },]

  const coloums = [
    {
      dataIndex: 'shoeCostStatus', key: 'shoeCostStatus', title: '状态', render: (text: any, record: Column) => (
        <p >
          {text ? '启用' : '停用'}
        </p>
      )
    },
    { dataIndex: 'shoeBoxMaterialSku', key: 'shoeBoxMaterialSku', title: '物料SKU', width: 160 },
    { dataIndex: 'shoeBoxMaterialName', key: 'shoeBoxMaterialName', title: '鞋盒物料名称' },
    { dataIndex: 'shoeBoxSpecification', key: 'shoeBoxSpecification', title: '内盒规格' },
    { dataIndex: 'shoeBoxUnitPrice', key: 'shoeBoxUnitPrice', width: 110, title: '鞋盒单价', },
    {
      dataIndex: 'shoe', key: 'shoe', title: '鞋子配置', render: (text: any, record: Column) => (
        <p >
          {record.processCategory}/{record.packCategory}/{record.shoePackCount}/{record.transportationRoute}
        </p>
      )
    },
    { dataIndex: 'outBoxSpecification', key: 'outBoxSpecification', width: 200, title: '外箱规格' },
    { dataIndex: 'outBoxUnitPrice', key: 'outBoxUnitPrice', width: 90, title: '外箱单价' },
    { dataIndex: 'freightSplit', key: 'freightSplit', width: 160, title: '运费平摊/双' },
    { dataIndex: 'outBoxUnitPricePerDouble', key: 'outBoxUnitPricePerDouble', width: 160, title: '外箱平摊/双' },
    { dataIndex: 'totalAmount', key: 'totalAmount', width: 120, title: '总价/双' },
    {
      dataIndex: 'record', key: 'record', title: '操作', width: 120, render: (text: any, record: Column) => (
        <Button onClick={() => showDrawer(record.shoeCostConfigurationId)}>
          日志
        </Button>
      )
    },
  ] as Column[];

  // 字典接口
  const getDict = async () => {
    const craft = await DictApi.getDict(23)
    setCraftOption(craft.data)
    const pack = await DictApi.getDict(21)
    setPackOption(pack.data)
    const packNum = await DictApi.getDict(22)
    setPackNumOption(packNum.data)
    const transport = await DictApi.getDict(20)
    setTransportOption(transport.data)
  }

  // 日志抽屉开始
  const showDrawer = (id: string) => {
    setOpen(true);
    setId(id);
  };

  // 日志抽屉结束
  const onClose = () => {
    setOpen(false);
  };

  // 获取列表
  const getDataList = async (data: PageInfo) => {
    const parmas = { ...pageData, ...data }
    const res = await ShoeApi.getList(parmas)
    setTotal(res.data.total);
    setData(res.data.list.map((i) => ({ ...i, key: i.shoeCostConfigurationId })));
    setLoad(false);
  };

  // 页码改版
  const pageChange = (pageData: PageInfo) => {
    let data = searchForm.getFieldsValue();
    getDataList({ ...pageData, ...data });
    setPageData(pageData);
  };


  // 复选框选中
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: object[]) => {
      setSelectedRowKeys(selectedRowKeys)
      setSelectData(selectedRows)
    }
  };

  // 导出
  const handExport = () => {
    const num = selectedRowKeys.length > 0 ? selectedRowKeys.length : total
    confirm({
      title: `请确认，本次共导出${num}条数据`,
      icon: <ExclamationCircleFilled />,
      content: '',
      onOk() {
        let data = searchForm.getFieldsValue();
        ShoeApi.export({ ...data, ...pageData, ids: selectedRowKeys }).then((res: any) => {
          message.success(res.msg)
        })
      }
    });
  }

  const handStatus = (status: number) => {

    confirm({
      title: `确认执行鞋成本${status ? "启用" : "停用"}操作吗？`,
      icon: <ExclamationCircleFilled />,
      content: '',
      onOk() {
        const check = selectData.every((item: any) => { return item.shoeCostStatus === status ? 0 : 1 })

        if (check) {
          ShoeApi.updateStatus({ ids: selectedRowKeys, type: status }).then((res: any) => {
            message.success(res.msg)
            getDataList(pageData)
          })

        } else {
          message.warning(`只能${status ? "启用" : "停用"}${status ? "停用" : "启用"}状态的鞋成本，请重新操作`)
        }

      }
    })
  }

  const handAdd = () => {
    navigate('/database/newshoeCost')
  }

  useEffect(() => {
    if (typeof getDict === "function") {
      getDict();
    }
  }, []);

  const tableTop = (
    <Row justify="space-between" gutter={80} className="operation-btn">
      <Col style={{ lineHeight: "32px" }}>
        <Button type="primary" onClick={handAdd}>
          新增鞋成本配置
        </Button>
        <Button type="primary" onClick={handExport} disabled={selectedRowKeys.length === 0}>
          编辑鞋成本配置
        </Button>
        <Button type="primary" onClick={() => handStatus(1)} disabled={selectedRowKeys.length === 0}>
          启用
        </Button>
        <Button type="primary" onClick={() => handStatus(0)} disabled={selectedRowKeys.length === 0}>
          停用
        </Button>
      </Col>
      <Col>
        <Button type="primary" onClick={handExport}>
          导出
        </Button>
      </Col>
    </Row>
  );

  return (<div>
    <Spin spinning={load}>
      <MySearch setPageData={setPageData} getDataList={getDataList} searchData={searchData}></MySearch>
      <MyTable
        rowSelection={{
          type: 'checkbox',
          ...rowSelection,
        }}
        title={() => tableTop}
        dataSource={tableData}
        columns={coloums}
        pagination={false}
        saveKey="MyListSearch"
      />
      <MyPagination
        page={pageData.page}
        immediately={getDataList}
        change={pageChange}
        total={total}
      />
    </Spin>
    <MyDrawer open={open} onClose={onClose} id={id}></MyDrawer>
  </div>)
}

ShoeCostList.route = { [MENU_PATH]: "/database/shoeCost" };