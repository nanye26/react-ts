import { useEffect, useState } from "react";
import { Form, Button, Row, Col, Spin, message, Modal } from "antd";
import { ExclamationCircleFilled } from '@ant-design/icons';
import React from 'react';
import { Steps } from 'antd';
import './index.less'
const description = 'This is a description.';
const { confirm } = Modal;
export default function NewShoeCost() {

  return (<div>

    <Steps
      direction="vertical"
      items={[
        {
          title: 'Finished',
          description,
        },
        {
          title: 'In Progress',
          description,
        },
        {
          title: 'Waiting',
          description,
        },
      ]}
    />

  </div>)
}

NewShoeCost.route = { [MENU_PATH]: "/database/newshoeCost" };
