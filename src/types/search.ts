export interface SearchTypes {
  getDataList: Function
  setPageData: Function
  searchData: Array<any>
}