import { UserInfo } from "./user"
import { MenuItem, MenuList } from "./menu"
import { number } from "echarts"
export type MessageList = MessageItem[]
type MessageItem = {
  add_time: string
  creator: string
  description: string
  m_id: number
  name: string
}
export interface ResponseData {
  code: number
  msg?: string
  data?: any
}

export interface ListInfo {
  list: any[]
  pages: number
  prePage: number
  total: number
}

export interface MessageAPi extends ResponseData {
  data?: {
    total: number
    list: MessageList
  }
}

export interface LoginData extends ResponseData {
  data: UserInfo
}
export interface ListData extends ResponseData {
  data: ListInfo
}

export type PowerList = {
  type_id: number
  name: string
  menu_id: string
}[]

export interface PowerApi extends ResponseData {
  data: PowerList
  menu: MenuList
}

export interface MenuInfoApi extends ResponseData {
  data: MenuItem | null
}

export type ResponseUserInfo = {
  account: string
  pswd: string
  type: string
  user_id: number
  username: string
}
export interface UserListApi extends ResponseData {
  data: {
    list: ResponseUserInfo[]
  }
  total: number
}

type TimeInfo = {
  time: string
  value: number
}
export interface VisitorApi extends ResponseData {
  data: {
    deal: TimeInfo[]
    ips: TimeInfo[]
    today: {
      deal: number
      ips: number
    }
  }
}

export type VisitData = {
  ip: string
  s_id: number
  status: string
  time: string
  url: string
}

export interface VisitorListApi extends ResponseData {
  data: {
    list: VisitData[]
    total: number
  }
}

export type LogData = {
  className: string
  createTime: string
  id: number
  methodName: string
  operate: string
  operateContent: string
  thirdpartId: string
  userName: string
}

export interface LogApi extends ResponseData {
  data: LogData
}