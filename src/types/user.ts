// 用户信息
export type UserInfo = {
  phone: string
  token: string
  nickName: string
  userAccount: string
  userPhone: string
  userId: string
  userType: string
} | null

export type UserAction = {
  type: string
  info?: UserInfo
}

export type UserState = {
  user: UserInfo
}