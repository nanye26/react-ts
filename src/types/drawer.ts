export interface DrawerTypes {
  open: boolean
  onClose: ((e: React.MouseEvent<Element, MouseEvent> | React.KeyboardEvent<Element>) => void)
  id: string
}