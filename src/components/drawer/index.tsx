import { Card, Drawer, Timeline } from 'antd';
import PropTypes from 'prop-types';
import { DrawerTypes } from '@/types/drawer';
import './index.less'
import DictApi from '@/api/dict';
import { useEffect, useState } from 'react';
import { LogData } from '@/types/api';
export default function MyDrawer(props: DrawerTypes) {

  const [logData, setLogData] = useState<any>([])

  const getData = async () => {
    const res = await DictApi.selectLogById({ id: props.id })
    setLogData(res.data)
  }

  const setStyle = () => {
    // @ts-ignore
    document.querySelector('body').className = props.open ? 'dialog-open' : ''
  }

  useEffect(() => {
    if (!props.id) return
    getData()
  }, [props.id])


  useEffect(() => {
    setStyle()
  }, [props.open])


  return (
    <>
      <Drawer
        title="操作日志"
        placement='right'
        width={500}
        onClose={props.onClose}
        open={props.open}
      >
        <Timeline>
          {
            logData.map((item: LogData, index: number) => {
              return (
                <Timeline.Item key={index}>
                  <p>{item.createTime}</p>
                  <Card title="" style={{ width: 400 }}>
                    【{item.userName}】在<span style={{ color: '#409eef' }}>【{item.className}】</span>操作了【{
                      item.methodName
                    }】{item.operateContent}
                  </Card>
                </Timeline.Item>
              )
            })
          }
        </Timeline>
      </Drawer>
    </>
  );
};


MyDrawer.propsType = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
};