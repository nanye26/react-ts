import React, { useEffect, useState } from "react";
import { Row, Pagination } from "antd";
import "./index.less";
const pageSizeOptions = ["20", "50", "100"];

export type PageInfo = { page: number, pageSize: number | string }

interface PaginationProps {
  total: number
  page: number
  change: (p: PageInfo) => void
  immediately?: (p: PageInfo) => void
}

export default function MyPagination({ total, page = 1, change, immediately }: PaginationProps) {
  const [pageSize, setSize] = useState<string>(pageSizeOptions[0]);

  useEffect(() => {
    if (typeof immediately === "function") {
      immediately({ page, pageSize });
    }
    // eslint-disable-next-line
  }, []);

  const pageChange = (page: number, pageSize: number) => {
    setSize(pageSize + '' || pageSizeOptions[0]);
    if (typeof change === "function") {
      change({ page, pageSize });
    }
  };

  return (
    <Row justify="end" className="pagination-wapper">
      <Pagination
        total={total}
        showSizeChanger
        showQuickJumper
        onChange={pageChange}
        showTotal={(total) => `共 ${total} 条`}
        pageSizeOptions={pageSizeOptions}
        defaultPageSize={20}
      />
    </Row>
  );
}
