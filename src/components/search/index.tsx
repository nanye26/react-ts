import { SearchTypes } from "@/types/search";
import {
  Form,
  Input,
  Button,
  Row,
  Col,
  Select,
} from "antd";
import PropTypes from 'prop-types';
import { useState } from "react";
import './index.less'

export default function MySearch(props: SearchTypes) {
  const [form, setForm] = useState<any>();
  const [searchForm] = Form.useForm();

  // 顶部搜索
  const search = (type: boolean) => {
    let data = {}
    if (!type) {
      data = searchForm.getFieldsValue();
      setForm(data);
    } else {
      data = form
    }
    props.setPageData({ page: 1, pageSize: 20 })
    props.getDataList(data);
  };

  const handleChange = (e: any, item: any) => {
    console.log(e);

    setForm({ ...form, [item.prop]: e.target.value.trim() })
    console.log(form);

  }

  return (
    <div className="top-form">
      <Form layout="inline" form={searchForm} labelCol={{ span: 5 }}>
        <div className="form-left">
          <Row>
            {
              props.searchData.map((item: any) => (
                <Col span={6} key={item.prop}>
                  <Form.Item name={item.prop} label={item.label} >
                    {item.type === 'input' && <Input placeholder={item.placeholder} onChange={(e) => handleChange(e, item)} />}
                    {item.type === 'select' && (<Select placeholder={item.placeholder} onChange={(e) => handleChange(e, item)}>
                      {item.options.map((i: any) => (
                        <Select.Option value={i[item.valueName] || i.value} key={i[item.valueName] || i.value}>{i[item.labelName] || i.label} </Select.Option>
                      ))}
                    </Select>)}
                  </Form.Item>
                </Col>
              )
              )
            }
          </Row>
        </div>

        <div className="form-right">
          <Button onClick={() => search(true)} type="primary" className="submit-btn">
            搜索
          </Button>
          <Button
            onClick={() => {
              searchForm.resetFields();
              search(false);
            }}
          >
            清空
          </Button>
        </div>
      </Form>
    </div>
  )
}

MySearch.propsType = {
  setPageData: PropTypes.func.isRequired,
  getDataList: PropTypes.func.isRequired,
  searchData: PropTypes.array.isRequired
}